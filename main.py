from bs4 import BeautifulSoup
import requests
import sqlite3

conn = sqlite3.connect("db.sqlite3", check_same_thread=False)
curs = conn.cursor()


def Skillbox(direction):
    html = ''
    if 'Prog' in direction:
        html = BeautifulSoup(requests.get('https://skillbox.ru/code/?type=course').text, 'lxml')
    elif 'Mark' in direction:
        html = BeautifulSoup(requests.get('https://skillbox.ru/marketing/?type=course').text, 'lxml')
    n = html.find_all('a', class_='card__title h h--4')
    names = [i.text.replace('\xa0', ' ').replace('\n', ' ') for i in n]
    hrefs = [i['href'] for i in n]
    times = [i.text.replace('\n', '') for i in html.find_all('b', class_="card__count")]
    abouts, costs = [], []
    for i in hrefs:
        h = BeautifulSoup(requests.get(i).text, 'lxml')
        abouts.append(h.find('p', class_="start-screen__desc").text)
        try:
            costs.append(h.find('span', class_="h h--3").text + ' ₽/мес.')
        except AttributeError:
            pass
        try:
            costs.append(h.find('span', class_="h h--2").text + ' ₽/мес.')
        except AttributeError:
            pass

    for name, href, time, about, cost in zip(names, hrefs, times, abouts, costs):
        curs.execute(
            "INSERT INTO courses VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')".format(name, href, time, about, cost))
        conn.commit()


def Kursfinder():
    html = BeautifulSoup(requests.get('https://kursfinder.ru/school/hwschool-online/').text, 'lxml')
    names = [i.text.replace('\n', '').replace('  ', '') for i in html.find_all('div', class_="course-card__title title")]
    hrefs = ['https://kursfinder.ru/link/kurs-dev-python-hwschool' + i['href'] for i in html.find_all('a', class_="course-card__link red-btn card__btn")]
    times = [i.text for i in html.find_all('span', class_="course-card__duration")]
    costs = [i.text.replace('\n', '').replace('  ', '') for i in html.find_all('span', class_="course-card__price")]

    for name, href, time, cost in zip(names, hrefs, times, costs):
        curs.execute(
            "INSERT INTO courses VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')".format(name, href, time, 'None', cost))
        conn.commit()


def Uprav():
    html = BeautifulSoup(requests.get('https://uprav.ru/marketing/').text, 'lxml')
    n = [i for i in html.find_all('a', class_="catalog-newest-card__title")]
    names = [i.text for i in n]
    hrefs = ['https://uprav.ru/' + i['href'] for i in n]
    costs = [i.text for i in html.find_all('div', class_="catalog-newest-card__price")]

    for name, href, cost in zip(names, hrefs, costs):
        curs.execute(
            "INSERT INTO courses VALUES ('{0}', '{1}', '{2}', '{3}', '{4}')".format(name, href, 'None', 'None', cost))
        conn.commit()


def make_all(direction):
    if 'Prog' in direction:
        Skillbox(direction)
        Kursfinder()
    elif 'Mark' in direction:
        Skillbox(direction)
        Uprav()
    return 0


def main(d):
    make_all(d)
    print('OK')


if __name__ == '__main__':
    main(input('Choose a direction: Programming | Marketing\n'))
#https://bitbucket.org/VladimirNovashov/workspace/projects/HAH